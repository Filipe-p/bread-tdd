
## Indentation in python defines the start of a block of code.
## in other languages this is define with {} or with `do` an `end` or other key word/characters
## Python uses indentation. So the start and end of the function is defined using identation.

# print('outside the block of code above')

def make_dough(arg1, arg2, arg3):
    # How to make it so order doesn't matter
    # create a list, check if the 3 ingredient exit 
    print('inside block')
    ingedient_list = [arg1.lower().strip(), arg2.lower().strip(), arg3.lower().strip()]
    correct_list = ['flour', 'yeast', 'water']
    if correct_list[0] in ingedient_list and correct_list[1] in ingedient_list and correct_list[2] in ingedient_list:
        return 'dough'
    else:
        return 'not dough'


# print('outside the block of code  bellow')


# make_dough('hey', 'you', 'ingredient')