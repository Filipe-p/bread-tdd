# Pão Alentejano Bread Factory TDD :Bread:

This exercise will use TDD to make a simple bread factory in Python. 
We'll also apply SCRUM and Agile principles. 

Learning outcomes: 

- Git & Bitbucket
- Documentation
- TDD
- Functions 
- SCUM & AGILE 


### TDD - Test driven Development 

![alt text](https://marsner.com/wp-content/uploads/test-driven-development-TDD.png)

TDD is test driven Development. We'll do the Tests first, then the functions. 

- Reduces technical debt
- ensure maintainability
- ensures working code 

### Functions

Functions are like people at work: They should have **ONE JOB!** and do that job!

This makes them easy to Test. 

They shoul keep your code *DRY* Don't Repeat Yourself. 

A function **should not print** - Because the output will not be usable by other programs. 

### SCRUM and AGILE

We have look at Ceremonies and at User stories.

This time we'll add:

- Acceptance criterion
- Definition of Done (DoD)

**Acceptance criterion** is for INDIVIDUAL CARDS. Is from what you build TESTs. It helps ensure the user story's function works correctly in the wider context of the program. And ofcourse by itself. 

Example: Adding a sink to Zac's house: 1) does it have running water? 2) does it have sewage? 

**Definition of Done (DoD)** Applies transversly to all card. Its a teams definition that a card has been done, and has to be defined WITH THE PRODUCT OWNER. 

Example: Adding a sink to Zac's house: 1) did you clean the debree 2) did you remove the old sink

### TASK and User stories

I want to make bread. Imagine this was a functions to controll the factory. 


#### User story 1

As a factory controller, I want a function that takes in water, flour and yeast, so that it can make dough.

#### User story 2

As a factory controller, I want a function for baking that takes in dough, so that it can make bread. 

#### User story 3

As a factory controller, I want a function that is called run factory that just takes in the necessary engridents and makes the bread. 
