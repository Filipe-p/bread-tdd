from bread_functions import *
# arg1 = "hello"
# # Test one assert if arg1 = hello
# print('testing if arg one = hello')
# print(arg1 == "hello")

# Test that when given water, flour and yeast it output "dough"
print('test 1')
print("Test that when given water, flour and yeast it output 'dough'")
print('result:', make_dough('water', 'flour', 'yeast') == 'dough')


# Any other ingredients should out "not dough"
print('test 2')
print("Any other ingredients should out 'not dough'")
print('result:', make_dough('water', 'cement', 'yeast') == 'not dough')


# Test that function not case sensitive
print('test 3')
print("Test that when given WATER, FLour and yEASt it output 'dough'")
print('result:', make_dough('WATER', 'FLour', 'yEASt') == 'dough')


# Test that function accepts ingredients with trailing whitespaces
print('test 4')
print("Test that when given WATER     ,        FLour and     yEASt      it output 'dough'")
print('result:', make_dough('WATER      ', '     FLour', '     yEASt     ') == 'dough')

# Test that function accepts ingredients in any order
print('test 5')
print("Test that when given water, flour and yeast in wrong order output 'dough'")
print('result:', make_dough('flour', 'yeast', 'water') == 'dough')
print('result:', make_dough('yeast', 'water', 'flour') == 'dough')